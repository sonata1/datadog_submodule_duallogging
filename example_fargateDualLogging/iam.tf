data "aws_caller_identity" "main" {}

# ECS assume role policy template
data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

# ECS Exec policy
data "aws_iam_policy_document" "ecs_exec" {
  statement {
    resources = ["*"]
    actions = concat((module.datadog_sidecar).execution_actions, (module.datadog_sidecar).task_actions, [
      "ssmmessages:CreateControlChannel",
      "ssmmessages:CreateDataChannel",
      "ssmmessages:OpenControlChannel",
      "ssmmessages:OpenDataChannel",
    ])
  }
}

# IAPI Execution Role
resource "aws_iam_role" "ecs" {
  name                 = "${var.name_prefix}-datadog"
  permissions_boundary = "arn:aws:iam::${data.aws_caller_identity.main.id}:policy/GELBoundary"
  assume_role_policy   = data.aws_iam_policy_document.assume_role.json
}

# IAPI Role Attachment
resource "aws_iam_role_policy_attachment" "ecs" {
  role       = aws_iam_role.ecs.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

# Attach ECS policy to the role
resource "aws_iam_role_policy" "ecs" {
  name   = "${var.name_prefix}-ecs"
  role   = aws_iam_role.ecs.id
  policy = data.aws_iam_policy_document.ecs_exec.json
}
