variable "application" {
  type    = string
  default = "silvapacheDual"
}

variable "name_prefix" {
  type = string
  default = "fargate_sidecarDual_3"
}

variable "task_definition_cpu" {
  type = string
  default = "512"
}

variable "task_definition_memory" {
  type = string
  default = "1024"
}

variable "environment" {
  type = string
  default = "dev"
}