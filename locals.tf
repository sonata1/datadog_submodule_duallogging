locals {
  default_tags = {
    Environment  = var.environment
    Organization = "Genomics England"
    Provisioner  = "Terraform"
    Service      = var.application
    Component    = "Orchestration"
    Sourcepath   = "terraform/"
  }
  execution_actions = [
    "cloudwatch:*",
    "logs:CreateLogGroup",
    "logs:CreateLogStream",
    "logs:PutLogEvents",
    "logs:DescribeLogStreams",
  ]
  task_actions = [
    "secretsmanager:GetSecretValue",
    "ssm:GetParameters",
    "kms:Decrypt",
    "kms:ListKeys",
    "kms:ListAliases",
    "kms:DescribeKey"
  ]
  merged = [for container_definition in var.container_definitions : merge(container_definition, local.log_configuration)]
}
