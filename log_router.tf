resource "aws_cloudwatch_log_group" "log_group" {
  name              = var.application
  tags              = local.default_tags
  retention_in_days = 1
}


locals {
  log_router_definition = {
    name  = "log_router"
    image = var.log_router_image
    user  = "0"
    logConfiguration = {
      logDriver = "awslogs"
      options = {
        awslogs-group         = var.awslogs_group,
        awslogs-region        = var.region
        awslogs-stream-prefix = "firelens"
        awslogs-create-group  = "true"
      }
    }
    firelensConfiguration = {
      type = "fluentbit"
      options = {
        enable-ecs-log-metadata = "false"
        config-file-type        = "file"
        config-file-value       = "/dual_logging.conf"
      }
    },
    essential = true
    environment = [
      {
        name  = "DD_TAGS"
        value = join(" ", [for k, v in var.datadog_tags : join(":", k, v)])
      },
      {
        name  = "DD_SERVICE"
        value = var.application
      },
      {
        name  = "LOG_STREAM_GROUP"
        value = var.application
      }
    ],
    secrets = [
      {
        name = "DD_API_KEY"
        valueFrom = var.datadog_api_key
      }
    ]
  }
}